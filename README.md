﻿##SpatialKey Software Engineer, Front End - Interview Exercise

Kathy is an Underwriter.  Her job is to assess the risk of a given location, along with other factors, to make decisions about pricing policies. Typically she is only given the address of a location and must manually cross-reference an address against different Risk Modelers maps, one by one, to determine the level of risk. This is done in the browser and across different websites for each Risk Modeler. Since each Risk Modeler has their own methodology for determining the risk, she likes to view the results side by side for comparison, so she copies her findings into a spreadsheet. She also likes to see where the location is on the map to gain confidence in the results. She is only interested in United States locations (see **Example US Addresses** below).

You are tasked with unifying Kathy’s current process into a single page application. Thankfully, you stumbled upon the SpatialKey API and it has exactly what you need. You’ve been given a rough wireframe to give you an idea of what the app might look like. It’s up to you to use it or not. You’ve also got a nearly barebones index.html file to work from. Feel free to add any UI enhancements you think are important for Kathy to get her job done, but make sure you don’t spend all your time moving pixels.

**Example US Addresses**

* 1201 18th Street, Suite 250 Denver, CO 80202
* 400 S Monroe St, Tallahassee, FL 32399
* 1600 Amphitheatre Parkway Mountain View, CA 94043


##Wireframe
![wireframe](./wireframe.png)
  
#Requirements
* Must use the SpatialKey API
* Must be written in HTML, CSS and JavaScript
* You may use any libraries or frameworks or none at all
* You may use any mapping library of your choice to display the location on the map
* If there are any instructions for building your project or any other details you'd like to elaborate on include your own README (feel free to rename the existing file)
* You must provide an estimated completion date that will be used to schedule your follow up interview, during which you will demo your application
* When you are finished and ready to submit your work push to an branch on this repository and submit a pull request to the master branch which will notify us and provide a mechanism to review your application and aide the discussion in the followup interview.

##API Docs


Authentication (POST)
https://hireme.spatialkey.com/SpatialKeyFramework/api/v2/session.json

Post Data:
```
 email: ui-candidate@spatialkey.com
 password: skNewHire2017
 orgName: hireme
```


Note: We’ve already hardcoded this call into index.html, feel free to leave as is, or expand upon it in your own way.


Geocoding (GET)
https://hireme.spatialkey.com/SpatialKeyFramework/api/v3/geocode/default.json?combined={fullAddress}


Params: 
* fullAddress: Some String Address


Returns: 
* An Array of GeoJSON Objects (http://geojson.org/geojson-spec.html#geojson-objects), if there are more than one, you can use the first.


Example: 
```JSON
[
  {
    "properties": {
      "country": "US",
      "city": "Tallahassee",
      "street": "400 S Monroe St",
      "postalCode": "32399-6526",
      "county": "Leon",
      "sk_location_granularity": -10,
      "state": "FL",
      "geocoder": "common"
    },
    "geometry": {
      "type": "Point",
      "coordinates": [
        -84.2819,
        30.438117
      ]
    },
    "type": "Feature"
  }
]
```

Hazard Lookup (GET)
https://hireme.spatialkey.com/SpatialKeyFramework/api/v1/hazard/18226c009f954220aa254015c85cae2c/{latitude}/{longitude}.xml


Params:
* latitude: Should be returned in the geocoding call above
* longitude: Should be returned in the geocoding call above


Returns:
* An XML Payload. Again, this returns XML not JSON.


**The values Kathy needs to compare are:**

* sk_analysis.result_values.sk_custom_us_fema_flood_risk (For FEMA)
* sk_analysis.result_values.sk_custom_us_katrisk_10m_flood_label (For KatRisk)

**Remember these will only come back if you are looking at a US location**