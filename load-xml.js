var mymap = L.map('mapid');


function search() {
    var address = document.getElementById('search-text').value;
    if (address.length === 0 || address === null) {
        toastr.error('Please enter a valid address');
    } else {
        getData(address, mymap);
    }
}

function getData(address, mymap) {

    var xhr = new XMLHttpRequest();
    var url = 'https://hireme.spatialkey.com/SpatialKeyFramework/api/v3/geocode/default.json?combined=' + address;
    xhr.open('POST', 'https://hireme.spatialkey.com/SpatialKeyFramework/api/v2/session.json');
    xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');

    xhr.withCredentials = true;

    xhr.onload = function () {
        if (xhr.status !== 200) {
            toastr.info('Ugh, the request failed :: ' + xhr.status);
        }
    };

    xhr.send(
        JSON.stringify({
            email: "rotimi.babalola@spatialkey.com",
            password: "1stEngage2017",
            orgName: "hireme"
        })
    );

    xhr.open('GET', url, true);
    xhr.send();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var addressData = JSON.parse(xhr.responseText);
            var coordinates = addressData[0].geometry.coordinates;

            mymap.setView([coordinates[1], coordinates[0]], 13);

            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoicm90aW1pLWJhYmFsb2xhIiwiYSI6ImNqNm1pY2dwZDBwazkycW9kcnhjcmVwdjMifQ.1FhTlN0YJwOF0Sl_I2m8HQ', {
                attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
                maxZoom: 18,
                id: 'mapbox.streets',
                accessToken: 'pk.eyJ1Ijoicm90aW1pLWJhYmFsb2xhIiwiYSI6ImNqNm1pY2dwZDBwazkycW9kcnhjcmVwdjMifQ.1FhTlN0YJwOF0Sl_I2m8HQ'
            }).addTo(mymap);

            L.marker([coordinates[1], coordinates[0]]).addTo(mymap);

            var xhr_2 = new XMLHttpRequest();
            var url_2 = `https://hireme.spatialkey.com/SpatialKeyFramework/api/v1/hazard/18226c009f954220aa254015c85cae2c/${coordinates[1]}/${coordinates[0]}.xml`;

            xhr_2.open('POST', 'https://hireme.spatialkey.com/SpatialKeyFramework/api/v2/session.json');
            xhr_2.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
            xhr_2.withCredentials = true;
            xhr_2.send(
                JSON.stringify({
                    email: "rotimi.babalola@spatialkey.com",
                    password: "1stEngage2017",
                    orgName: "hireme"
                })
            );

            xhr_2.open('GET', url_2, true);
            xhr_2.send();
            xhr_2.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    var xmlDoc = xhr_2.responseXML;
                    var femaLabel = xmlDoc.getElementsByTagName("sk_analysis")[0].childNodes[3].childNodes[3].innerHTML;
                    var katRiskLabel = xmlDoc.getElementsByTagName("sk_analysis")[0].childNodes[3].childNodes[8].innerHTML;

                    document.getElementById('fema-label').innerHTML = '<b>US fema flood risk:</b> ' + femaLabel;
                    document.getElementById('katrisk-label').innerHTML = '<b>Katrisk flood label:</b> ' + katRiskLabel;
                }
            }
        }
    };
};